package com.epcc.export.ehess.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.epcc.export.ehess.impl.repository.ExportMshSpecification;
import com.epcc.export.ehess.impl.repository.SearchCriteria;
import com.epcc.export.ehess.model.LdapExport;
import com.epcc.export.ehess.repository.IExportMshRepository;
import com.epcc.export.ehess.util.TraitementUtil;


@Service
public class ExportMshService {
	Logger logger = LoggerFactory.getLogger(ExportMshService.class);
	
	@Autowired
	IExportMshRepository exportMshRepository;
	@Value("${folderLocation}")
	private String folderLocation;
	@Value("${modifyCompte}")
	private String modifyCompte;
	//@Value("${expireCompte}")
	//private String expireCompte;
	//@Value("${templateExpire}")
	//private String templateExpire;
	@Value("${templatemodify}")
	private String templatemodify;
	@Value("${filePropertie}")
	private String filePropertie;
	
	public void findModifyCompteByDate(String date) {
		logger.info(" ************** compt expiré depuis 2 jours   ex :  ********* "+date);
		ExportMshSpecification spec = new ExportMshSpecification(new SearchCriteria("modifytimestamp", ">=", date));
		//List<Compte> results = accesGedRepository.findAll(spec);
		List<LdapExport> results = exportMshRepository.findAll();
		logger.info("nbre de compte modifié : "+results.size());
		logger.info("file properties = "+filePropertie);
		List<LdapExport> compteValid = results.stream()
				  .filter(valide->valide.getFdbadge()!=null)
				  .collect(Collectors.toList());
		logger.info("size compte valide = "+compteValid.size());

		// determine le profile de l'utilisateur
		List<LdapExport> compteUsers = getProfile(compteValid,null);
		
		if(compteUsers.size()>0) {
			createTemplateCompteExpired(compteUsers,modifyCompte,templatemodify);
		}else {
			logger.info(" Il n'y pas d'utilisateur dont le compte est modifié au moins 2 jours ");
		}
	}
	
	private List<LdapExport> getProfile(List<LdapExport> results, Properties props) {
		
		List<LdapExport> compteWithProfil = new ArrayList<LdapExport>();
		Set<LdapExport> compteNonExporte = new HashSet<LdapExport>();
		Properties properties = new Properties();
		
			try {
				FileInputStream fis = new FileInputStream(new File(filePropertie));
				properties.load(fis);
								
			}catch (IOException e) {
				logger.error("fichier non trouvé "+e);
			}			

		String[] listEtablissement = properties.get("alma.etablissement").toString().split(",");
		String[] listprofilEhess = properties.get("export.profil.ehess").toString().split(",");

		for (LdapExport profileCompte : results) {
			
			if(profileCompte.getCreatetimestamp()!= null) {
				profileCompte.setCreatetimestamp(profileCompte.getCreatetimestamp().replace("-", ""));
					
			}
			if(!(null == profileCompte.getFdcontractenddate())) {
				profileCompte.setFdcontractenddate(profileCompte.getFdcontractenddate().replace("-", ""));
			}
			StringBuilder buf = TraitementUtil.deleteFromSupannentiteaffectation(profileCompte);
			if (buf.length() == 0){
				compteNonExporte.add(profileCompte);
				logger.error("ExportMshSErvice : Pas  de suppanentiteaffectation pour : "+profileCompte.getMail());
			}else {
				List<String> lAffectation = TraitementUtil.getStringToList(buf);
				if(!(profileCompte.getSupannentiteaffectationprincipale() == null) && 
						ArrayUtils.contains(listEtablissement, profileCompte.getSupannentiteaffectationprincipale().trim())){
					// determination des profils 
					
					/** Si Badge et O=EHESS-Resident = Profile 2
					Si badge et o=EPCC = Profile 3
					Si badge et O=CNRS ou EHESS ou ENC ou FMSH ou HAP ou Ined ou Paris 1 ou Paris 3 ou Paris 8 ou Paris 10 ou Paris 13 
					sauf OU=GED-LECTEURS => profile 4 */
					
					if(profileCompte.getO().equals(properties.get("profil.ehess.resident").toString())){
						profileCompte.setPersonalTitle("2");
					}
					else if(profileCompte.getO().equals("EPCC")){
						profileCompte.setPersonalTitle("3");
					}
					else if(ArrayUtils.contains(listprofilEhess, profileCompte.getO()) && (!"GED-LECTEURS".equals(profileCompte.getOu())) ){
						profileCompte.setPersonalTitle("4");
						// sinon quel profil ? exemple : camille.masclet O = FMSH OU = GED-LECTEURS
					}else {
						logger.error("pas de profil pour : "+profileCompte.getUid()+ " groupe O = "+profileCompte.getO() + " OU = "+profileCompte.getOu() );
						compteNonExporte.add(profileCompte);
						profileCompte.setPersonalTitle("");
					}
											
				}else {
					logger.error("l'établissement : " +profileCompte.getSupannentiteaffectationprincipale() + " n'est pas un établissement membre valide");
					compteNonExporte.add(profileCompte);
				}
				if(!profileCompte.getPersonalTitle().toString().isEmpty()) {
					compteWithProfil.add(profileCompte);					
				}
				/*
				 * if ((null != profileCompte.getPersonalTitle().toString() )|| (!profileCompte.getPersonalTitle().toString().isEmpty())) {
					compteWithProfil.add(profileCompte);					
				}else {
					logger.info("is null : "+profileCompte.getUid());
				}
				 */
			}
		}
		logger.info("le nombre de compte non exporté est : "+compteNonExporte.size());
		return compteWithProfil;
	}

	private void createTemplateCompteExpired(List<LdapExport> results,String typeExport,String templateType) {
		
		/**
		 * Initialize engine and get template
		 */
		
		try{
			Properties p = new Properties();	
			p.setProperty("resource.loader", "class");
			p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
			p.setProperty("input.encoding", CharEncoding.UTF_8);
			p.setProperty("output.encoding",CharEncoding.UTF_8);
			p.setProperty("response.encoding",CharEncoding.UTF_8);
			p.setProperty("default.contentType", "text/html; charset=UTF-8");
	
			Velocity.init( p ); 
			VelocityContext context = new VelocityContext();
			
			Template template = Velocity.getTemplate("templates/"+templateType);
			template.setEncoding(CharEncoding.UTF_8);
			// récupération de la template
			if(template != null) {
				logger.info("template is loaded");
				context.put("userList", results);
				context.put("total", results.size());
				
				// chargement de la template
				loadTemplateT(template, context,typeExport);
			}
		}catch( ResourceNotFoundException rnfe ){
			logger.error("la ressource demandée n'existe pas " +rnfe);
		}catch( ParseErrorException pee ) {
			logger.error("une erreur est survenue lors du parsing de la template " +pee);
		}catch( MethodInvocationException mie ){
			logger.info("erreur est apparue lors d'un appel à une directive "+mie);
	
		}
		logger.info("Fin createTemplateXml  dans la classe mère");
	}
	
	private void loadTemplateT(Template template, VelocityContext context,String type) {
		try {
			//hr-YYYYMMJJ-HHMM
			String FORMAT = "hr-%1$tY%1$tm%1$td-%1$tH%1$tM";
			//20210315_13h27m21s_epcc_modifs.csv
			Calendar cal = Calendar.getInstance(); cal.setTime(Date.from(Instant.now()));
			
			String format = String.format( FORMAT, cal);
			StringBuilder builder = new StringBuilder();
			builder.append(folderLocation);
			builder.append(format);
			builder.append(type);
			FileWriter writer = new FileWriter(new File(builder.toString()));
			
			template.merge(context, writer);
			
			
			writer.flush();
			writer.close();
			File fileCreated = new File(folderLocation+format+type);
			if(fileCreated.exists()) {
				logger.info("file is created");
			}else {
				logger.error("could not created");
			}
			
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("le fichier n'est pas créé "+e.getMessage());
		}catch (FileAlreadyExistsException  e) {
			logger.error("file already exist");
		}catch (Exception e) {
			logger.error("");
		}
		
	}



}
