package com.epcc.export.ehess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.epcc.export.ehess.model.LdapExport;


public interface IExportMshRepository extends JpaRepository<LdapExport, Long>, JpaSpecificationExecutor<LdapExport>{

}
