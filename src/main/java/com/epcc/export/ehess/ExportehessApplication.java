package com.epcc.export.ehess;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.epcc.export.ehess.service.ExportMshService;


@SpringBootApplication
public class ExportehessApplication implements CommandLineRunner {
	Logger logger = LoggerFactory.getLogger(ExportehessApplication.class);

	@Autowired
	ExportMshService service;
	
	public static void main(String[] args) {
		SpringApplication.run(ExportehessApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("*** start Export EHESS ");
		int i = args.length;
		logger.info(" nbre d'argument  = "+i);
		String dateModifouExport= "";
		
		for(int j=0; j < args.length;j++) {
			dateModifouExport = args[0];
			logger.info("argument numero  ["+j+" ] : " +args[j]);	
		}
	
		// on prend tout le monde 
		//dateModifouExport="20190715";
		logger.info("date de modification  : "+dateModifouExport);
	
		service.findModifyCompteByDate(dateModifouExport);
		logger.info("fin de l'exécution du programme");
		
		
	}
}
